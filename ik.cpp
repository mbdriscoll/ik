#include <iostream>
#include <utility>
#include <list>
#include <vector>

#include <cstdio>
#include <cstdlib>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

#define GLM_SWIZZLE
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/io.hpp>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>

using namespace glm;

namespace gui {
float fov = 45.0,
      near = 0.01,      far = 500,
      width = 1980,      height = 1080,
      prev_x = 0,       prev_y = 0;

int time = 0,
    mouse[7] = {0, 0, 0, 0, 0, 0, 0};

vec3 eye(7, 2, 3),
     center(0,1,0),
     up(0,1,0),
     track_dx,
     track_dy;

bool animate = true;
bool draw_axes = true;
bool draw_derivs = true;
bool draw_frames = false;
bool draw_targets = true;
bool draw_residuals = true;
};

vec3 origin(0,0,0),
     axis_x(1,0,0),
     axis_y(0,1,0),
     axis_z(0,0,1);

void draw_frame(float len) {
    if (gui::draw_frames) {
        float old_width; glGetFloatv(GL_LINE_WIDTH, &old_width);
        glLineWidth(2);
        glDisable(GL_LIGHTING);
        glBegin(GL_LINES);
        glColor3f(1,0,0);   glVertex3f(0,0,0.01); glVertex3f(len, 0, 0+0.01);
        glColor3f(0,1,0);   glVertex3f(0,0,0.01); glVertex3f(0, len, 0+0.01);
        glColor3f(0,0.8,1); glVertex3f(0,0,0.01); glVertex3f(0, 0, len+0.01);
        glEnd();
        glEnable(GL_LIGHTING);
        glLineWidth(old_width);
    }
}

class SceneNode {
protected:
    SceneNode* parent;
    std::list<SceneNode*> children;

public:
    SceneNode(SceneNode *parent=NULL) :
        parent(parent), children()
    {
        if (parent)
            parent->children.push_back(this);
    }

    void draw_children() {
        std::list<SceneNode*>::iterator n_it;
        for (n_it = children.begin(); n_it != children.end(); n_it++)
            (*n_it)->draw();
    }

    virtual void draw() {
        draw_children();
    }

    void update_children(int time) {
        std::list<SceneNode*>::iterator n_it;
        for (n_it = children.begin(); n_it != children.end(); n_it++)
            (*n_it)->update(time);
    }

    virtual void update(int time) {
        update_children(time);
    }

    virtual mat4 pos() {
        return mat4(1);
    }
};

class Locator : public SceneNode {

public:
    Locator(SceneNode* parent=NULL)
        : SceneNode(parent)
    {  }

    virtual mat4 matrix() {
        return mat4(1);
    }

    virtual void draw() {
        glPushMatrix();
        glMultMatrixf(value_ptr(inverse(pos())));
        draw_frame(0.25);
        glPopMatrix();

        SceneNode::draw();
    }

    virtual mat4 pos() {
        if (parent)
            return matrix() * parent->pos();
        else
            return matrix();
    }
};

class StaticLocator : public Locator {
    mat4 _matrix;

public:
    StaticLocator(vec3 dir=axis_y, vec3 up=axis_z, SceneNode* parent=NULL)
        : Locator(parent)
    {
        _matrix = lookAt(dir, vec3(0), -up);
    }

    virtual mat4 matrix() {
        return _matrix;
    }
};

GLfloat boneColor[4] = {.8, .4, 1, 1};

std::vector<vec3>  positions;
std::vector<vec3>  axes;
std::vector<float> angles;
std::vector<int>   parents;

std::vector<vec3>  rest_positions;
std::vector<vec3>  rest_axes;

std::vector<vec3>  goals;
std::vector<int>   effectors;
std::vector<vec3>  residuals;
std::vector<vec3>  Jacobian;

void fill_jacobian() {
    int N = effectors.size();
    Jacobian.assign(Jacobian.size(), vec3(0)); // zero it
    for (int i = 0; i < effectors.size(); i++) {
        vec3 s = positions[effectors[i]];
        for (int j = effectors[i]; j >= 0; j = parents[j]) {
            vec3 jxyz;
            int parent = parents[j];
            if (parent == -1)
                jxyz = cross(axes[j], s-vec3(0));
            else
                jxyz = cross(axes[j], s-positions[parent]);
            Jacobian[i+N*j] = jxyz;
        }
    }
}

class Skeleton : public Locator {
public:
    Skeleton(SceneNode *parent=NULL) :
        Locator(parent)
    {  }

    virtual void update() {
        fill_jacobian();
    }

    virtual void draw() {
        glPushMatrix(); // 1
        glMultMatrixf(value_ptr(inverse(pos())));

        glEnable(GL_LIGHTING);
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, boneColor);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, boneColor);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, boneColor);

        for (int i = 0; i < positions.size(); i++) {
            vec3 b = positions[i],
                 p = (parents[i] == -1) ? vec3(0) : positions[parents[i]];

            vec3 segment = b-p;

            glPushMatrix(); // 1
            glTranslatef(p.x, p.y, p.z);

            // illustrate axis
            if (gui::draw_axes) {
                glPushMatrix(); // 3
                glDisable(GL_LIGHTING);
                glColor3f(1,1,0);
                glLineWidth(3);
                glBegin(GL_LINES);
                glVertex3f(0,0,0);
                glVertex3fv(value_ptr(0.25f*axes[i]));
                glEnd();
                glLineWidth(1);
                glPopMatrix(); // 3
            }

            // illustrate bones
            glPushMatrix(); // 2
            float len = length(segment);
            if (len > 0) {
                mat4 M = lookAt(segment, vec3(0), axis_z);
                glMultMatrixf(value_ptr(inverse(M)));
                glTranslatef(0,0,-len);
                draw_frame(0.25);
                glPushAttrib (GL_ALL_ATTRIB_BITS);
                glColor3f (.4, .2, .5);
                glEnable (GL_POLYGON_OFFSET_FILL);
                glDisable(GL_LIGHTING);
                glPolygonOffset (1., 1.);
                glutWireSphere(0.1, 8, 8);
                glutWireCone(0.1, len, 8, 8);
                glPopAttrib ();
                glEnable(GL_LIGHTING);
                glutSolidSphere(0.1, 8, 8);
                glutSolidCone(0.1, len, 8, 8);
            }
            glPopMatrix(); // 2

            glPopMatrix(); // 1
        }

        // illustrate residual
        if (gui::draw_residuals) {
            glDisable(GL_LIGHTING);
            glColor3f(1,0,0);
            glBegin(GL_LINES);
            for (int i = 0; i < effectors.size(); i++) {
                vec3 b = positions[ effectors[i] ];
                glVertex3fv(value_ptr(b));
                glVertex3fv(value_ptr(b + residuals[i]));
            }
            glEnd();
        }

        // illustrate Jacobian
        if (gui::draw_derivs) {
            glColor3f(0,1,0);
            glBegin(GL_LINES);
            int N = effectors.size();
            for (int i = 0; i < effectors.size(); i++) {
                vec3 b = positions[ effectors[i] ];
                for (int j = effectors[i]; j >= 0; j = parents[j]) {
                    vec3 db = Jacobian[i+N*j];
                    glVertex3fv(value_ptr(b));
                    glVertex3fv(value_ptr(b + db));
                }
            }
            glEnd();
        }

        // illustrate goals
        if (gui::draw_targets) {
            glDisable(GL_LIGHTING);
            glColor3f(1,0,0);
            glPointSize(10);
            glBegin(GL_POINTS);
            for (int i = 0; i < goals.size(); i++)
                glVertex3fv(value_ptr(goals[i]));
            glEnd();
        }

        glPopMatrix(); // 1
    }
};

class Stage : public SceneNode {
public:
    Stage() : SceneNode() {}

    virtual void draw() {
        glDisable(GL_LIGHTING);
        glColor3f(.9,.9,.9);
        glBegin(GL_LINES);
        for (int i = 0; i <= 10; i++) {
            glVertex3f(i-5, 0, -5); glVertex3f(i-5, 0, 5);
            glVertex3f(-5, 0, i-5); glVertex3f(5, 0, i-5);
        }
        glEnd();
        draw_children();
    }
};


Stage scene;
Skeleton root(&scene);

void ik_init() {
    positions.push_back( vec3(0,0,0) ); // 0
    axes     .push_back( vec3(1,0,0) );
    angles   .push_back(           0 );
    parents  .push_back(          -1 );

    positions.push_back( vec3(0,1,0) ); // 1
    axes     .push_back( vec3(0,0,1) );
    angles   .push_back(           0 );
    parents  .push_back(           0 );

    positions.push_back( vec3(0,1,0) ); // 2
    axes     .push_back( vec3(1,0,0) );
    angles   .push_back(           0 );
    parents  .push_back(           1 );

    positions.push_back( vec3(0,2,0) ); // 3
    axes     .push_back( vec3(0,0,1) );
    angles   .push_back(           0 );
    parents  .push_back(           2 );

    positions.push_back( vec3(0,2,0) ); // 4
    axes     .push_back( vec3(1,0,0) );
    angles   .push_back(           0 );
    parents  .push_back(           3 );

    positions.push_back( vec3(0,3,0) ); // 5
    axes     .push_back( vec3(0,0,1) );
    angles   .push_back(           0 );
    parents  .push_back(           4 );

    positions.push_back( vec3(0,3,0) ); // 6
    axes     .push_back( vec3(1,0,0) );
    angles   .push_back(           0 );
    parents  .push_back(           5 );

    positions.push_back( vec3(0,4,0) ); // 7
    axes     .push_back( vec3(0,0,1) );
    angles   .push_back(           0 );
    parents  .push_back(           6 );

    for (int i = 0; i < axes.size(); i++)
        axes[i] = normalize(axes[i]);

    rest_positions = std::vector<vec3>(positions);
    rest_axes      = std::vector<vec3>(axes);

    effectors.push_back(7);
    goals.push_back(vec3(0));
    residuals.push_back( vec3(0) );

    Jacobian.resize(effectors.size() * angles.size(), vec3(0));
}

float twonorm(std::vector<vec3> &v) {
    double ssum = 0.0;
    for (int i = 0; i < v.size(); i++) {
        ssum += v[i].x * double(v[i].x);
        ssum += v[i].y * double(v[i].y);
        ssum += v[i].z * double(v[i].z);
    }
    return std::sqrt(ssum);
}

#define TOLERANCE 1e-5
#define MAX_ITERS 100

void ik_update() {
#if 1
    goals[0] = vec3(0+3*sin((gui::time + 45) * M_PI / 180),
                    2+1*sin( gui::time * M_PI / 180),
                    0+5*cos((gui::time + 45) * M_PI / 180));
#else
    goals[0] = 5.0f * normalize(vec3(1,1,1));
#endif

    int nd = 2;
    int n = angles.size(),
        m = effectors.size() * 3;
    npy_intp nmdims[2] = {n, m},
             ndim[1]   = {n},
             mdim[1]   = {m};

    PyObject *pModuleName, *pModule, *pIkEval, *pResult;
    pModuleName = PyString_FromString((char*)"ik");
    pModule = PyImport_Import(pModuleName); assert(pModule);
    pIkEval = PyObject_GetAttrString(pModule, (char*)"ik_eval"); assert(pIkEval);

    PyObject *J = PyArray_SimpleNewFromData(2, nmdims, NPY_FLOAT, &Jacobian[0]);
    PyObject *R = PyArray_SimpleNewFromData(1, mdim, NPY_FLOAT, &residuals[0]);
    PyObject *T = PyArray_SimpleNewFromData(1, ndim, NPY_FLOAT, &angles[0]);

    for (int i = 0; i < effectors.size(); i++)
        residuals[i] = goals[i] - positions[ effectors[i] ];
    float norm = twonorm(residuals);

    for (int iter = 0; iter < MAX_ITERS; iter++) {
        // capture current positions
        std::vector<vec3> old_positions(positions);
        std::vector<vec3> dE(positions.size(), vec3(0));

        // compute new angles
        pResult = PyObject_CallFunction(pIkEval, (char*)"iOOO", iter, J, T, R);
        PyErr_Print();

        float cond_J = PyFloat_AsDouble(pResult);

        // apply angle changes to position and axis vectors
        std::vector<mat4> xforms(positions.size(), mat4(1));
        for (int i = 0; i < positions.size(); i++) {
            int parent = parents[i];
            if (parent == -1) {
                xforms[i] = rotate(angles[i], rest_axes[i]);
                positions[i] = (xforms[i]*vec4(rest_positions[i],1)).xyz();
            } else {
                xforms[i] = xforms[parent] *
                    translate(rest_positions[parent]) *
                    rotate(angles[i], rest_axes[i]) *
                    translate(-rest_positions[parent]);
                positions[i] = (xforms[i] * vec4(rest_positions[i],1)).xyz();
                axes[i] = normalize((xforms[parent] * vec4(rest_axes[i],1)).xyz());
            }
        }

        for (int i = 0; i < effectors.size(); i++)
            residuals[i] = goals[i] - positions[ effectors[i] ];

        for (int i = 0; i < positions.size(); i++)
            dE[i] = positions[i] - old_positions[i];

        float norm_dE = twonorm(dE);
        float norm_R = twonorm(residuals);
        if (iter > 0) {
            //printf("%4d %g %g %g\n", iter, std::log10(norm_dE), cond_J, std::log10(norm_R - 1.0f));
            if (norm_dE < TOLERANCE) {
                //printf("- met tolerance threshold after %d iterations.\n", iter);
                break;
            }
        }

        fill_jacobian();
    }
}

void display_func() {
    using namespace gui;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_LIGHTING);
    glBegin(GL_QUADS);
    glColor3f(.078, .078, .078);
    glVertex3f(-1, -1, 1);
    glVertex3f( 1, -1, 1);
    glColor3f(.41, .48, .57);
    glVertex3f( 1,  1, 1);
    glVertex3f(-1,  1, 1);
    glEnd();

    glViewport(0, 0, width, height);

    mat4 persp = perspective(fov, width/height, near, far);
    mat4 view = lookAt(gui::eye, gui::center, gui::up);

    glMatrixMode(GL_PROJECTION); glLoadMatrixf(value_ptr(persp));
    glMatrixMode(GL_MODELVIEW);  glLoadMatrixf(value_ptr(view));

    GLfloat color[4] = {1, 1, 1, 1};
    GLfloat lposition[4] = {5, 5, 10, 1};
    GLfloat ambient[4] = {0.1f, 0.1f, 0.1f, 1.0f};
    GLfloat diffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat shininess = 25.0;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, lposition);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);

    scene.draw();

    glutSwapBuffers();
}

void motion_func(int x, int y) {
    using namespace gui;
    if (mouse[0]) {
        eye = center + rotate(eye-center, (prev_x-x)/100.f, vec3(0,1,0));
        eye = center + rotate(eye-center, (y-prev_y)/100.f,
                                   cross(eye-center, vec3(0,1,0)));
    } else if (mouse[1]) {
        vec3 dt = track_dx*(x-prev_x)/100.0f - track_dy*(y-prev_y)/100.0f;
        center += dt; eye += dt;
    } else if (mouse[2]) {
        eye = center + (eye-center)*(1.0f - (y-prev_y)/100.f - (x-prev_x)/100.0f);
    }

    prev_x = float(x);
    prev_y = float(y);
    glutPostRedisplay();
}

void mouse_func(int button, int state, int x, int y) {
    using namespace gui;
    mouse[button] = !state;
    prev_x = float(x);
    prev_y = float(y);

    if (!state && button == 1) {
        track_dx = normalize(cross(eye-center, vec3(0,1,0)));
        track_dy = normalize(cross(eye-center, track_dx));
    }
    glutPostRedisplay();
}

void idle_func() {
    if (gui::animate) {
        //gui::animate = 0;
        scene.update(gui::time++);
        ik_update();
        glutPostRedisplay();
    }
}

void reshape_func(int w, int h) {
    gui::width = float(w);
    gui::height = float(h);
}

void keyboard_func(unsigned char key, int x, int y) {
    switch (key) {
        case 'a': gui::draw_axes      = !gui::draw_axes;      break;
        case 'd': gui::draw_derivs    = !gui::draw_derivs;    break;
        case 'f': gui::draw_frames    = !gui::draw_frames;    break;
        case 't': gui::draw_targets   = !gui::draw_targets;   break;
        case 'r': gui::draw_residuals = !gui::draw_residuals; break;

        case 'q': glutLeaveMainLoop();                   break;
        case ' ': gui::animate = !gui::animate;          break;
        default: printf("Unrecognized key: %c.\n", key); break;
    }
    glutPostRedisplay();
}

void initGL() {
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable (GL_POLYGON_OFFSET_FILL);
    glPolygonOffset (1., 1.);
}

static PyObject*
emb_doit(PyObject *self, PyObject *args)
{
    return Py_BuildValue("i", 1337);
}

static PyMethodDef EmbMethods[] = {
    {"doit", emb_doit, METH_VARARGS,
        "Return the number of arguments received by the process."},
    {NULL, NULL, 0, NULL}
};


void initPython() {
    Py_SetProgramName((char*) "ik");
    Py_Initialize();
    import_array(); // numpy
    Py_InitModule("_ik", EmbMethods);
}

int main(int argc, char *argv[]) {
    initPython();

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA |GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(gui::width, gui::height);
    glutCreateWindow("IK");

    glutKeyboardFunc(keyboard_func);
    glutDisplayFunc(display_func);
    glutReshapeFunc(reshape_func);
    glutMotionFunc(motion_func);
    glutMouseFunc(mouse_func);
    glutIdleFunc(idle_func);

    initGL();
    ik_init();
    glutMainLoop();
}
