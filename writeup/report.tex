\documentclass[twocolumn]{article}

\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{color}
\usepackage[boxed]{algorithm2e}

\title{Numerical Stability of Iterative Solvers for Inverse Kinematics}
\author{Michael Driscoll}
\date{\today}

\begin{document}

\maketitle

\begin{abstract}
In its basic formulation, the inverse kinematics (IK) problem seeks to compute a set of joint angles such that the end of a given joint chain coincides with a certain position in space. Further constraints, such as orientation, velocity, and preferences for curvature, are possible, but beyond the scope of this project. In this report, we describe the basic forward and inverse kinematics problems, describe mathematical techniques for solving them, and present results from our implementations. In particular, we study the Jacobian Pseudoinverse and Damped Least Squares methods. They can be numerically unstable, so we focus on cases when such problems arise. We conclude that Damped Least Squares performs better for our benchmarks, and given mathematical intuition as to why this is the case.
\end{abstract}

\section{Introduction}
\label{sec:intro}

Inverse kinematics is an important problem in the fields of robotics and computer animation. In its general case, it seeks a particular configuration of a ``multi-body'' (e.g. set of bones connected by joints) such that the ``end effectors'' (e.g. hands) meet certain objectives, such as position in space, velocity, and/or angular velocity. In robotics, it might be used to position a robotic arm so its welding torch is in a desired position. In animation, it might be used to position a character's wrist, elbow, and shoulder so their hand is picking up a cup. The problem is complicated by the fact that desired positions can have many valid configurations, or none. In the latter case, we seek the configuration that best approximates the desired goal, for a definition of `best' to be discusses shortly.

\section{Problem Formulation}

Not surprisingly, Inverse Kinematics is the inverse of the Forward Kinematics (FK) problem. An FK function $f$ computes the position of a set of end effectors $ e$ given a vector of joint angles $\theta$:
$$ e = f(\theta).$$
For a given joint chain, it is straightforward to derive a procedure for $f$ using trigonometry and 4x4 transformation matrices.

For IK, we seek the inverse of $f$, i.e. the joint angles as a function of the target positions:
$$ \theta = f^{-1}(e). $$
For small joint chains, this can be derived analytically, but in general it eludes analysis. Therefore, we turn to numerical techniques to find a solution. We observe a few important characteristics of $f$ that inform our choice of methods:
\begin{itemize}
\item $f$ is nonlinear because it contains non-trivial sines and cosines.
\item $f$ is non-injective because some target positions may be reachable by different joint configurations.
\item $f$ is non-surjective because not every target position is reachable.
\end{itemize}

Despite these complications, $f$ is still relatively smooth---small changes in joint angles yield predictable changes in end effector positions. We exploit this fact to develop our solution.

We now formalize our notion of a solution, or an approximate solution in the case where the target position is unreachable. Using forward kinematics, we can compute the position of the end effectors given the current joint angles. The difference between the current position and the target position can be thought of as a residual---i.e. the distance and direction the end effectors must move to satisfy our goal. This can be represented:
$$ r = e - f(\theta) $$
Our objective then becomes clear---to minimize the norm of the residual. Since we're in 3D space, we use the $l2$ norm because of its geometric interpretation as distance. This choice implies a unique solution in cases where target positions are at or beyond the reach of the joint chain, and it selects a correct solution when many exist. We'd like to pick a canonical solution in the latter case (e.g. to preserve both temporal coherence and computation independence of frames in animation), but such work is beyond the scope of this paper.


\section{Mathematical Solutions}

The properties of $f$ hinder inversion, so we turn to an iterative method to find a solution. Since $f$ is relatively smooth, we can take its derivative and use it to move closer to our target position. Mathematically:
$$\Delta e = J(\theta) \cdot \Delta \theta.$$
Here, $J$ represents the Jacobian matrix of $e$ with respect to $\theta$. Each entry captures the partial change in an end effector position given a partial change in the corresponding joint angle. For a given set of joint angles, the Jacobian is:
$$ J_{ij} = \frac{\partial e_i}{\partial \theta{}_j}$$
If there are $m$ target positions and $n$ angles, the Jacobian is $m$-by-$n$. In practice, there are usually a few hundred angles, and tens of target positions, so the matrix is small by traditional linear algebra standards. Even though performance isn't a problem, stability usually is, so we press on.

We make the approximation that the change in end effector positions should be equal to the residual:
$$\Delta e = r = e - f(\theta).$$
Thus, we seek a $\Delta \theta$ that gives us the desired change in position. Ideally, we'd invert the Jacobian and be done
$$\Delta \theta = J^{-1}\Delta e,$$
but $J$ is not guaranteed to be invertible, or even square, so we seek other methods.

At this point, we compute an $n$-by-$m$ matrix that approximates the inverse of $J$. We use two approaches that we describe shortly, but for now assume that we've computed an ``update matrix'' $J^*$ that, when multiplied by $\Delta e$, gives us a reasonable $\Delta \theta$:
$$\Delta \theta = J^*\Delta e.$$
We add $\Delta \theta$ into $\theta$ and compute the new position and residual using FK. Hopefully we've moved closer, and we can repeat this process until we get close enough.

If the target position is attainable, then we can iterate until the $l2$ norm of the residual is within a certain tolerance, usually $10^{-5}$ in animation applications. However, the position may not be attainable, and in the presence of complicated constraints, we might not be able to determine if it's reachable. Therefore, we iterate until the distance the end effector moves between iterations is less that a given distance. The full algorithm is given in Algorithm \ref{alg} and is based on work described in \cite{buss} and \cite{watt}.

\begin{algorithm}[]
 \KwData{$e$, a vector of target positions.}
 \KwData{{\tt MAX\_ITER}, max number of iterations to try.}
 \KwData{{\tt THRESHOLD}, smallest distance to move and keep iterating.}
 \KwResult{$\theta$, a vector of joint angles.}
 \For{$i=1$ \KwTo {\tt MAX\_ITER}}{
   form Jacobian $J$ \\
   compute update matrix $J^*$ \\
   $\Delta \theta = J^* \Delta e$ \\
   $\Delta p = ||FK(\theta + \Delta \theta) - FK(\theta)||_2$ \\
   $\theta = \theta + \Delta \theta$ \\
   \If{$\Delta p < $ {\tt THRESHOLD}}{
      break;
   }
 }
 \caption{Our iterative method for IK.}
 \label{alg}
\end{algorithm}

\subsection{Pseudoinverse of the Jacobian}
We complete the description of our approach by describing the techniques used to compute the update matrix. We explored two variants, described here, that approximate an inverse to $J$.

Unsurprisingly, the Moore-Penrose pseudoinverse is a good fit.  We can show it's a reasonable choice via the normal equations:
\begin{align*}
J \Delta \theta &= \Delta e \\
J^T J \Delta \theta &= J^T \Delta e \\
J^T J \Delta \theta &= J^T \Delta e \\
\Delta \theta &= (J^T J)^{-1} J^T \Delta e \\
\Delta \theta &= J^{+} \Delta e \\
\end{align*}

The pseudoinverse approach is promising because it gives the least-squares solution to our problem, but it performs poorly when the Jacobian has low rank. Such deficiencies often arise when the joint chain stretches out and the axes of rotation align. In that case, the update directions are nearly identical and the rows of the Jacobian approach each other.

This instability can be illustrated by examining the singular value decomposition of the Jacobian. If $J = U\Sigma V^T$, then $J^+ = V\Sigma^{-1}U^T$. In particular, the singular values of $J^+$ are $\frac{1}{\sigma_i}$ (where $\sigma_i$ is the $i$-th singular value of $J$). When the singular values of $J$ approach zero, the updates become excessively large and the method fails to converge.

\subsection{Damped Least Squares}

One solution to avoid instability due to low rank is the Damped Least Squares method. Here, we attempt to minimize
$$||J\Delta \theta - e||_2 + \lambda^2||\Delta \theta||_2$$
which corresponds to solving
$$(J^TJ + \lambda^2I)\Delta \theta = J^T\Delta e.$$
If $J = U\Sigma V^T$ is an SVD, it can be shown
$$(J^TJ + \lambda^2I)^{-1}J^T = V\left[\left(\Sigma^2 + \lambda^2I\right)^{-1}\Sigma\right]U^T,$$
a matrix whose singular values take the form
$$\sigma_i' = \frac{\sigma_i}{\lambda^2 + \sigma_i^2}.$$
Thus, if the axes of rotation align and $J$'s singular values collapse to zero, the singular values of the update matrix don't explode but rather tend toward zero.

Thus, Damped Least Squares' update matrix takes the form
$$J^* = (J^TJ + \lambda^2 I)^{-1}J^T.$$

\section{Experiments}

We implemented both techniques in order to assess their stability. Our implementation uses Numpy's {\tt pinv} method to compute the pseudoinverse. For Damped Least Squares, we use Numpy's explicit inverse method, although any method for solving a linear system will suffice, and set $\lambda = 1$, which we found to give good results. Our experiments were performed in IEEE single-precision arithmetic.

Our test setup comprises a joint chain of eight joints, each with a single axis of rotation. To simulate ``ball'' joints, we group the joints into four coincident pairs with orthogonal axes of rotation. Our system supports any number of end effectors, but we only used one located at the end of the joint chain.

We study three cases which an IK solver must handle gracefully, including target positions with a unique solution, many valid solutions, and no valid solutions. For each, we plot three things:
\begin{itemize}
\item The convergence of $||\Delta e||_2$ toward our stopping criterion ($10^{-5}$). Intuitively, we stop when a given step doesn't move the end effector far from the previous step.
\item The convergence of the residual $r$. Our stopping criterion doesn't require that we have made progress toward the goal, so we plot $r$ to verify that our choice is reasonable. When solutions are unreachable, the residual will converge to an unknown, nonzero number, so we can't use it as our criterion. However, we know the ideal end position for our test cases (because we constructed them), so we adjust the plotted residuals accordingly.
\item The condition number of the Jacobian. $Cond(J)$ changes over the course of the method, so we plot the condition number to understand how it varies on methods that perform well.
\end{itemize}
In hindsight, a better stopping criterion might be $(||\Delta e||_2 < 10^{-5}$ or $||r||_2 < 10^{-5})$
because it aborts earlier if the target is reached. Nevertheless, our current stopping criterion leaves us within $10^{-5}$ when the method terminates, on all test cases.

Figures \ref{fig:one}, \ref{fig:inf}, and \ref{fig:zero} show the performance of each method. We observe that the Pseudoinverse method is unstable when no solution exists. The Damped Least Squares method maintains stability across all three cases, but performs slowly when only one solution exists. Therefore, we recommend using Damped Least Squares because such singularities are rare.

\begin{figure*}[h!]
\centering
\begin{subfigure}[b]{0.32\textwidth}
  \includegraphics[width=\textwidth]{one.png}
  \caption{One Solution}
  \label{img:configs:one}
\end{subfigure}
~
\begin{subfigure}[b]{0.32\textwidth}
  \includegraphics[width=\textwidth]{inf.png}
  \caption{Many Solutions}
  \label{img:configs:inf}
\end{subfigure}
~
\begin{subfigure}[b]{0.32\textwidth}
  \includegraphics[width=\textwidth]{zero.png}
  \caption{Zero Solutions}
  \label{img:configs:zero}
\end{subfigure}
\caption{The joint chain used in our experiments consisted of eight joints, grouped into four bones with two axes of rotation each. In these figures, the purple cones represent bones and the red dot represents the goal position. The green lines illustrate vectors in the Jacobian and capture how the position of the end effector changes given a change in each joint's $\theta$ (i.e. the partial derivative). Notice how the derivative vectors align in certain configurations corresponding to low-rank Jacobians.}
\label{img:one}
\end{figure*}

\begin{figure*}[t]
\centering
\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{de_o.eps}
\end{subfigure}
~
\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{res_o.eps}
\end{subfigure}

\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{cond_o.eps}
\end{subfigure}
\caption{Performance of a problem with a unique solution. The pseudoinverse method converges much faster than the Damped Least Squares method (20 vs 2,500 iterations).}
\label{fig:one}
\end{figure*}


\begin{figure*}[t]
\centering
\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{de_i.eps}
\end{subfigure}
~
\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{res_i.eps}
\end{subfigure}

\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{cond_i.eps}
\end{subfigure}
\caption{Performance of a problem with a many solutions. The methods are comparable, although Damped Least Squares is slightly faster.}
\label{fig:inf}
\end{figure*}


\begin{figure*}[t]
\centering
\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{de_z.eps}
\end{subfigure}
~
\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{res_z.eps}
\end{subfigure}

\begin{subfigure}[b]{0.49\textwidth}
  \includegraphics[width=\textwidth]{cond_z.eps}
\end{subfigure}
\caption{Performance of a problem with no valid solutions. The pseudoinverse method fails to converge, while the Damped Least Squares method converges rapidly.}
\label{fig:zero}
\end{figure*}

\section{Demo and Code}

{\color{blue} \href{https://bitbucket.org/mbdriscoll/ik}{Source code}} and a
{\color{blue} \href{https://vimeo.com/114626019}{demonstration video}} are available online.

\section{Conclusions}

We have compared two common methods for implementing inverse kinematics. The Pseudoinverse method shows converges quickly when target positions are attainable, but is unstable otherwise. The Damped Least Squares method can be slow to converge to unique solutions, but is robust and reasonably fast. In practice, Damped Least Squares seems like the best choice.

\bibliographystyle{acm}
\bibliography{report}

\end{document}
