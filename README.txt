Requires glm, python2, numpy. Edit the makefile so the header and library paths are correct.

Build via:
$ make

Run via:
$ PYTHONPATH=`pwd` ./ik
