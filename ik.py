import numpy as np
import numpy.linalg as la

#METHOD = "J+"
#METHOD = "JT"
METHOD = "DLS"

def ik_eval(i, JT, T, R):
    """
    i   iteration number
    JT  transpose of Jacobian matrix
    T   target
    R   residual
    """
    #print "- iteration %3d: cond(J)=%7.4f, norm(R)=%7.4f" % \
    #      (i, la.cond(JT), la.norm(R))

    J = JT.T

    if METHOD == "J+":
        J_plus = la.pinv(J)
        dT = np.dot(J_plus, R)

    elif METHOD == "JT":
        JJte = np.dot(J, np.dot(JT, R))
        alpha = np.inner(R, JJte) / np.inner(JJte, JJte)
        dT = alpha * np.dot(JT, R)

    elif METHOD == "DLS":
        lamda = 1.0
        dT = np.dot(JT, np.dot(la.inv(np.dot(J,JT) +
                        lamda**2*np.eye(J.shape[0])), R))

    T += dT

    return la.cond(J)
