NUMPY_INCLUDES = -I/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/numpy/core/include
PYTHON_INCLUDES = $(shell python2.7-config --includes) $(NUMPY_INCLUDES)
PYTHON_LDFLAGS = $(shell python2.7-config --ldflags)
CXXFLAGS = -I/opt/local/include $(PYTHON_INCLUDES)
LDFLAGS = -L/opt/local/lib -lGL -lGLU -lglut $(PYTHON_LDFLAGS)

default: ik
